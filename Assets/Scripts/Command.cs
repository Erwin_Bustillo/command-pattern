using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Command
{
    public abstract void Execute(Animator anim, bool forward);
}

public class PerformJump : Command
{
    public override void Execute(Animator anim, bool forward)
    {
        anim.SetTrigger("isJumping");
    }
}

public class DoNothing : Command
{
    public override void Execute(Animator anim, bool forward)
    {

    }
}

public class PerformKick : Command
{
    public override void Execute(Animator anim, bool forward)
    {
        anim.SetTrigger("isKicking");
    }
}

public class PerformPunch : Command
{
    public override void Execute(Animator anim, bool forward)
    {
        anim.SetTrigger("isPunching");
    }
}

public class MoveFoward : Command
{
    public override void Execute(Animator anim, bool forward)
    {
        if (forward)
            anim.SetTrigger("isWalking");
        else
            anim.SetTrigger("isWalkingR");
    }
}

